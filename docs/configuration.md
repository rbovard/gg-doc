---
sidebar_position: 5
---

# Configuration

This section lists all the configuration entries available in GeoGirafe, and how to configure them.  
The whole configuration is done in the `config.json` file.  
Every configuration option that has no default value must be set properly to make the application work.

## General

The `general` section contains all the common configuration entries that can be used by any component.

| Name     | Type    | Description                                                                                                                      | Default   | Examples             |
| -------- | ------- | ------------------------------------------------------------------------------------------------------------------------ ------- | --------- | -------------------- |
| `locale` | `String`| Locale used when formatting numbers or dates in the application. For example this locale will be used to format the coordinates. | `en-US`   | `de-CH`, `fr-FR`     |


## Languages

The `languages` section contains an option for each available language in the application plus other options is an `Array` of `language`.  

| Name              | Type     | Description                                                                                                                                                   | Default | Examples                                                                                                                                    |
| ----------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| `languages`       | `Object` | An object containing the list of available translations identified by their shortname, and with a link to the JSON file containing the translations as value. | -       | `"languages": {`<br/>` "de":"static/de.json",`<br/>` "en":"https://map.geo.bs.ch/static/X/en.json",`<br/>` "fr":"static/fr.json"`<br/>` }`  |
| `defaultLanguage` | `String` | The shortname of the default language that will be used when the application starts.                                                                          | -       | `en`, `fr`, `de`                                                                                                                            |

## Themes

The `themes` section contains the configuration options linked to the themes.

| Name                | Type      | Description                                                                                                                       | Default | Examples                                                                                   |
| ------------------- | --------- | --------------------------------------------------------------------------------------------------------------------------------- | ------- | -------------------------------------------------------------------------------------------|
| `url`               | `String`  | Link to the GeoMapFish-Compliant Themes.json                                                                                      | -       | `Mock/themes.json`, `https://map.geo.bs.ch/themes?background=background&interface=desktop` |
| `defaultTheme`      | `String`  | Name of the default theme, that will be automatically loaded on application start.                                                | `''`    | `main`, `cadastre`                                                                         |
| `imagesUrlPrefix`   | `String`  | URL Prefix to use when accessing themes images, if there are defined with relative paths in `themes.json`.                        | `''`    | `https://map.geo.ti.ch`                                                                    |
| `showErrorsOnStart` | `Boolean` | If `true`, the errors listed in `themes.json` will be displayed when the application starts. If `false`, they won't be displayed. | `false` | `true`, `false`                                                                            |

## Basemaps

The list of basemaps available in the application is loaded from the `themes.json` file.  
In addition, you can use the `basemaps` section to configure additional options.

| Name                   | Type      | Description                                                                                                                                         | Default  | Examples                              |
| ---------------------- | --------- | --------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ------------------------------------- |
| `show`                 | `Boolean` | Display the Basemap control or not. If set to `false`, the basemap controller won't be visible in the application.                                  | `true`   | `true`, `false`                       |
| `defaultBasemap`       | `String`  | Name of the default basemap, that will be displayed when the application starts. This is the name of the background map available in `themes.json`. | `''`     | `background_color`, `swissimage`      |
| `OSM`                  | `Boolean` | Enable or disable OpenStreetMap as an additional basemap layer.                                                                                     | `false`  | `true`, `false`                       |
| `SwissTopoVectorTiles` | `Boolean` | Enable or disable the VectorTiles layer from Swisstopo as an additional basemap layer.                                                              | `false`  | `true`, `false`                       |

## Treeview

The `treeview` section contains the configuration options for the layertree.

| Name                                | Type      | Description                                                                                                                                                                                      | Default | Examples         |
| ----------------------------------- | --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------- | ---------------- |
| `useCheckboxes`                     | `Boolean` | If `true`, checkboxes will be used in the treeview to show if a layer has been activated or not. If `false`, a simple bullet will be used.                                                       | `false` | `true`, `false`  |
| `useLegendIcons`                    | `Boolean` | If `true`, legend icon will be used in the treeview instead of the bullet, if any is defined in the metadata of the layer in the `themes.json` file. If `false`, the legend icons won't be used. | `false` | `true`, `false`  |
| `hideLegendWhenLayerIsDeactivated`  | `Boolean` | If `true`, the legend of a layer will automatically be hidden if the layer is not activated. If `false`, the legend will remain visible even if the layer is deactivated.                        | `true`  | `true`, `false`  |
| `defaultIconSize`                   | `Object`  | If `useLegendIcons` has been set to `true`, the size of the legend icons can be configured here with the `width` and `height` properties.                                                        | -       | -                |
| `defaultIconSize.width`             | `Integer` | Width of the icon that will be used for legend icons                                                                                                                                             | `20`    | `any integer`    |
| `defaultIconSize.height`            | `Integer` | Height of the icon that will be used for legend icons                                                                                                                                            | `20`    | `any integer`    |

## Search

The `search` section contains the configuration options for the search.  

| Name | Type   |                                                                                                                                                                                                                                                                                                                          | Default | Examples                                                                                                                                                    |
| ---- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------  | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
|`url` |`String`| Link to the GeoMapFish-Compliant Search Service. This service has to be compatible with the format of the GeoMapFish Search-Service. The link should contain the following strings:<br/>- `###SEARCHTERM###`: will be replaced by the text to search.<br/>- `###SEARCHLANG###`: will be replaced by the search language. | -       | `https://map.geo.bs.ch/search`<br/>`?limit=90`<br/>`&partitionlimit=15`<br/>`&interface=desktop`<br/>`&query=###SEARCHTERM###`<br/>`&lang=###SEARCHLANG###` |

## Print

The `print` section contains the configuration options for the print.  

| Name             | Type       | Description                                                                                                          | Default          | Examples                              |
|------------------|------------|----------------------------------------------------------------------------------------------------------------------|------------------|---------------------------------------|
| `url`            | `String`   | Link to the GeoMapFish-Compliant Print Service.                                                                      | -                | `https://map.geo.bs.ch/printproxy/`   |
| `formats`        | `String[]` | The desired print formats. The provided format must be supported by MapFishPrint. The order is kept in the dropdown. | `["png", "pdf"]` | `["png", "pdf", "jpg"]`               |
| `defaultFormat`  | `String`   | The print format selected by default. Only used if listed in `formats` and supported by MapFishPrint.                | -                | `"pdf"`                               |
| `layouts`        | `String[]` | A list of allowed layouts name. If configured, the layouts will be filtered using this value.                        | -                | `["1 A4 portrait", "2 A3 landscape"]` |
| `defaultLayout`  | `String`   | The print layout selected by default. Only used if the name match a layout.                                          | -                | `"1 A4 portrait"`                     |
| `scales`         | `Number`   | A list of allowed scales for every layouts. If configured, the scales will be filtered using this value.             | -                | `[50000, 25000, 10000, 2500, 1000]`   |
| `attributeNames` | `String`   | The print attributes (optional fields) to display as inputs in the panel. The order is kept in the panel.            | -                | `["legend", "title", "comments"]`     |
| `legend`         | `Object`   | The options for the print legend. See below.                                                                         | -                | `{...}`                               |


### Print Legend

The `print.legend` section contains the configuration options for the legend of the print.

| Name              | Type                                       | Description                                                                                                                                                                                     | Default | Examples                             |
|-------------------|--------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|--------------------------------------|
| `useExtent`       | `boolean`                                  | Use or not the bbox to get the wms legend. For QGIS server only.                                                                                                                                | `true`  | `false`                              |
| `showGroupsTitle` | `boolean`                                  | Display or not groups title in the legend. Switching to false is useful to obtains a "flat" legend.                                                                                             | `true`  | `false`                              |
| `label`           | `Object<string, boolean>`                  | The key is the server type (MapServer, QGIS, etc.), if the value is false the name of the layer will be not displayed. This is used to avoid duplicated title, as text and in the legend image. | -       | `{"mapserver": false}`               |
| `params`          | `Object<string, <Object<string, unknown>>` | The key is the server type (MapServer, QGIS, etc.) or image for an URL from a metadata. The value is some additional parameters set in the query string.                                        | -       | `{"mapserver": {"filter": "house"}}` |


## Share

The `share` section contains the configuration options for the shortlinks.  

| Name       | Type    | Description                                                                                                                                                                                                           | Default | Examples             |
| -----------| ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- | -------------------- |
|`service`   |`String` | Type of ShortUrl service we want to use. Supported configuration are:<br/>- `gmf`: the GeoMapFish backend shortUrl service will be used.<br/>- `lstu`: the open-source service <Let's Shorten That URL> will be used. | `gmf`   | `gmf`, `lstu`        |
|`createUrl` |`String` | The URL that will be used to create the shortUrl.                                                                                                                                                                     | -       | `https://lstu.fr/a`  |

## Bookmarks

The `bookmarks` section contains the configuration options for saving the bookmarks. It the `service` type is `server` the bookmarks will be fetched and posted as JSON.

| Name       | Type    | Description                                                                           | Default        | Examples                                   |
| -----------| ------  | --------------------------------------------------------------------------------------| -------------- | ------------------------------------------ |
|`service`   |`String` | Type type of service beeing used                                                      | `localStorage` | `localStorage`, `server`                   |
|`get`       |`String` | Optional URL that will be used to get the bookmarks if the service type is `server`   | -              | `https://yourbackendserver.com/bookmarks`  |
|`post`      |`String` | Optional URL that will be used to save the bookmarks if the service type is `server`  | -              | `https://yourbackendserver.com/bookmarks`  |

## Selection

The `selection` section contains the configuration options for the selected/focused features on the map.

| Name                     | Type      | Description                                                                  | Default       | Examples                              |
| ------------------------ | --------- | ---------------------------------------------------------------------------- | ------------- | ------------------------------------- |
| `defaultFillColor`       | `String`  | Default fill color used when selecting an object in the map.                 | `#ff66667f`   | `any color value in #RVBA format`     |
| `defaultStrokeColor`     | `String`  | Default stroke color used when selecting an object in the map.               | `#ff3333`     | `any color value in #RVBA format`     |
| `defaultStrokeWidth`     | `Integer` | Default stroke width used when selecting an object in the map.               | `4`           | `any integer`                         |
| `defaultFocusFillColor`  | `String`  | Default fill color used when focusing an object in the map.                  | `#ff33337f`   | `any color value in #RVBA format`     |
| `defaultFocusStrokeColor`| `String`  | Default stroke color used when focusing an object in the map.                | `#ff0000`     | `any color value in #RVBA format`     |
| `defaultFocusStrokeWidth`| `Integer` | Default stroke width used when focusing an object in the map.                | `4`           | `any integer`                         |

## Redlining

The `redlining` section contains the configuration options when drawing features on the map.

| Name                | Type    | Description                                                 | Default       | Examples                           |
| ------------------- | ------- | ----------------------------------------------------------- | ------------- | ---------------------------------- |
| `defaultFillColor`  | `String`| Default fill color used when drawing features on the map.   | `#6666ff7f`   | `any color value in #RVBA format`  |
| `defaultStrokeColor`| `String`| Default stroke color used when drawing features on the map. | `#0000ff`     | `any color value in #RVBA format`  |
| `defaultStrokeWidth`| `Number`| Default stroke width used when drawing features on the map. | `2`           | `any integer`                      |
| `defaultTextSize`   | `Number`| Default text size used when drawing features on the map.    | `12`          | `any integer`                      |
| `defaultFont`       | `String`| Default font used when drawing features on the map.         | `Arial`       | `Arial`, `Tahoma`, `Verdana`       |

## Projections

The `projections` section contains the list of projection that are available in the application.

| Name          | Type     | Description                                                                | Default  | Examples                                                                                                                      |
| ------------- | -------- | -------------------------------------------------------------------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------- |
| `projections` | `String` | An array containing the list of `EPSG` coordinate systems with their name. | -        | `"projections": {`<br/>&nbsp;`"EPSG:3857":"W-M",`<br/>&nbsp;`"EPSG:4326":"WGS84",`<br/>&nbsp;`"EPSG:2056":"LV95"`<br/>`}`     |


## Map

The `map` section contains the configuration of the `OpenLayers` map.

| Name               | Type      | Description                                                                                                                         | Default  | Examples                                                                                |
| ------------------ | --------- | ----------------------------------------------------------------------------------------------------------------------------------- | -------- | --------------------------------------------------------------------------------------- |
| `srid`             | `String`  | Defqult SRID of the map.                                                                                                            | -        | `EPSG:2056`                                                                             |
| `scales`           | `Array`   | Array of scales allowed on the map.                                                                                                 | -        | `[200000, 75000, 40000, 20000, 10000, 7500, 5000, 3500, 2000, 1000, 500, 200, 100, 50]` |
| `startPosition`    | `String`  | Initial center position when the application starts.                                                                                | -        | `2611000,1267000`                                                                       |
| `startZoom`        | `Integer` | Initial zoom level when the application starts.                                                                                     | -        | `any valid integer for the map zoom level`                                              |
| `maxExtent`        | `String`  | Maximum extent for the map.                                                                                                         | -        | `2583000,1235000,2650000,1291000`                                                       |
| `constrainScales`  | `Boolean` | If `true`, the map will only be allowed to used the specific scales defined in `maps.scales`.<br/>If `false`, any scale is allowed. | `true`   | `true`, `false`                                                                         |
| `showScaleLine`    | `Boolean` | Show scale line on the map, or not.                                                                                                 | `true`   | `true`, `false`                                                                         |

## Map3D

The `map3d` section contains the configuration of the `Cesium` globe.

| Name                                 | Type      | Description                                                                                                                                                                                                                                      | Default  | Examples                                                                                                                |
| ------------------------------------ | --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------- | ----------------------------------------------------------------------------------------------------------------------- |
| `terrainUrl`                         | `String`  | URL for the 3D map terrain.                                                                                                                                                                                                                      | -        | `https://terrain100.geo.admin.ch`<br/>`/1.0.0/ch.swisstopo.terrain.3d/`                                                 |
| `terrainImagery`                     | `Object`  | -                                                                                                                                                                                                                                                | -        | -                                                                                                                       |
| `terrainImagery`<br/>`.url`          | `String`  | the URL of your imagery provider (WMTS in your case), with template parameters as described in the [cesium documentation](https://cesium.com/learn/cesiumjs/ref-doc/UrlTemplateImageryProvider.html?classFilter=UrlTemplateImageryProvider#url). | -        | `https://wmts20.geo.admin.ch/1.0.0`<br/>`/ch.swisstopo.swissimage-product/default/`<br/>`current/4326/{z}/{x}/{y}.jpeg` |
| `terrainImagery`<br/>`.minLoD`       | `Integer` | The minimal level-of-detail that the imagery provider supports, if not mentioned, when zooming very far, the map may try to load nonexistent tiles that cause network error.                                                                     | -        | `any integer that representes a valid level-of-details`                                                                 |
| `terrainImagery`<br/>`.maxLoD`       | `Integer` | Same as `minLoD` but for maximal level-of-detail, to prevent loading nonexistent tiles when zooming very close.                                                                                                                                  | -        | `any integer that representes a valid level-of-details`                                                                 |
| `terrainImagery`<br/>`.coverageArea` | `Array`   | The rectangle, in degree, that describe the area provided by your WMTS. This parameter prevents requesting tiles that do not exist on the WMTS.                                                                                                  | -        | `[5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741]`                                         |
| `tilesetsUrls`                       | `Array`   | Array of URLs for 3D map tilesets.                                                                                                                                                                                                               | -        | `["https://3d.geo.bs.ch/static/tiles/a57b8783-1d7b-4ca9-b652-d4ead5436ead_2/tileset.json"]`                             |
