---
sidebar_position: 1
---

# Contributing

Welcome to the GeoGirafe project! We appreciate and encourage contributions from individuals and organizations to enhance the project's development and maintenance. Here are various ways you can get involved:

# Become member of the GeoMapFish Community

GeoGirafe is a project of the GeoMapFish community. You can join it by becoming a member of the GeoMapFish-User-Group, and connect with other enthusiasts, share your experiences, and stay updated on the latest developments in the GeoGirafe project.  
Visit our website for more details: https://geomapfish.org/community/

# Financing

Support the growth and sustainability of GeoGirafe by contributing to its development and maintenance efforts.  
If you are interested in co-financing or providing financial assistance, please reach out to us at contact@geomapfish.org.

# Developing

If you have ideas or contributions to improve the GeoGirafe project, or if you possess development capabilities that could benefit the project, we welcome your involvement. Connect with us to discuss your proposals and explore collaboration opportunities.

# Helping fix issues

Thank you for considering contributing to the GeoGirafe project. Your contributions will play a crucial role in enhancing the overall quality of this open-source project.  
Contribute to the improvement of GeoGirafe by identifying and addressing issues.  
Report any problems or bugs you encounter on our GitLab repository: https://gitlab.com/geogirafe/gg-viewer/-/issues/new  

# Discuss and chat with us

To ease fluidity on discussions around development of this open source project, or to get on board easily, we invite you to join us on the dedicaced discord server : https://discord.gg/WNSeVekN.
