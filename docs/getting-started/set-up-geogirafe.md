# Set up a GeoGirafe client

If you're new to GeoGirafe, this guide is the perfect place to start.

:::tip

GeoGirafe and this tutorial are written in TypeScript. However, it doesn't mean your project must be in TypeScript. We use [Vite](https://vitejs.dev/) to bundle our project. Please ensure you follow the "[Getting Started](https://vitejs.dev/guide/)" guide for Vite before proceeding.

:::

## Requirements

* Node.js version 18+ or 20+

## Install

Create a TypeScript project with Vite:

```sh
npm create vite@latest my-geogirafe-app -- --template vanilla-ts
cd my-geogirafe-app
```

Install geogirafe-lib

```sh
npm install @geogirafe/lib-geoportal
```

Run the development server to ensure everything is working:

```sh
npm run dev
```

You should see a demo app from Vite featuring a demo counter. Let's remove everything related to the counter:

* Delete useless files:
  ```sh
  rm src/counter.ts
  rm src/typescript.svg
  ```
* Remove everything inside `src/main.ts`.

## Configuration

The first step is to configure GeoGirafe.

Instead of creating a config file from scratch, download one:

```sh
curl https://gitlab.com/geogirafe/gg-viewer/-/raw/main/demo/config.c2c.json -o config.json
```

:::tip

You can find all the documentation about this configuration file [here](../configuration).

:::

GeoGirafe is designed to work with a [backend](./install-geomapfish) but we can mock one for this tutorial.

Let's create a `Mock` folder:

```sh
mkdir Mock
```

Download existing translation files into the `Mock` folder. These files provide translations for the interface as well as for your layers and attributes:

```sh
curl "https://geomapfish-demo-2-8.camptocamp.com/static/dummy/de.json" -o Mock/de.json
curl "https://geomapfish-demo-2-8.camptocamp.com/static/dummy/en.json" -o Mock/en.json
curl "https://geomapfish-demo-2-8.camptocamp.com/static/dummy/fr.json" -o Mock/fr.json
```

Finally, let's create a very simple layer tree to display on our first map. Create a `Mock/themes.json` file and copy-paste the following inside:

```json
{
  "themes": [],
  "background_layers": [
    {
      "id": 2,
      "name": "OSM map",
      "type": "WMTS",
      "url": "https://geomapfish-demo-2-8.camptocamp.com/tiles/1.0.0/WMTSCapabilities.xml",
      "matrixSet": "epsg2056_005",
      "layer": "map",
      "imageType": "image/jpeg"
    }
  ]
}

```

## First map

After adding a number of files, no map is visible yet. It's time to create our first beautiful map!

Add the following to your `src/main.ts` file:

```typescript
import './style.css';
import proj4 from 'proj4';
import { register } from 'ol/proj/proj4';
import { BasemapComponent, MapComponent, TreeViewComponent } from '@geogirafe/lib-geoportal/components';
import { ThemesManager } from '@geogirafe/lib-geoportal/tools';

// Definition of the swiss projection
proj4.defs(
  'EPSG:2056',
  '+proj=somerc +lat_0=46.9524055555556 +lon_0=7.43958333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs +type=crs'
);
register(proj4);

// Initialise GeoGirafe themes, il will read the themes.json we created before
ThemesManager.getInstance();

// Define the three minimal webcomponents needed to show the map
customElements.define('girafe-map', MapComponent);
customElements.define('girafe-basemap', BasemapComponent);
customElements.define('girafe-tree-view', TreeViewComponent);

```

Now that we defined three webcomponents, we can use them in our `index.html`:

```html
<body>
  <girafe-tree-view hidden></girafe-tree-view>
  <girafe-map></girafe-map>
  <script type="module" src="/src/main.ts"></script>
</body>
```

For this first part of the tutorial, we will focus on the map. That's why `girafe-tree-view` is hidden.

Lastly, to view your map in full screen replace the content of your `style.css` file with:

```css
html, body {
  margin: 0;
}
```

Run the Vite server (if not already running):

```sh
npm run dev
```

And that's it, you should now be able to see your first map!
